import time, sqlite3, os
from time import gmtime, strftime

user_dont_exist = "NO SUCH USER"
enter_login  = "LOGIN:"
req_login = "LOGIN!"
enter_password = "PASS:"

db_name = "temp.db"

def logo():
    print("""

      /$$$$$$   /$$$$$$  /$$      /$$ /$$$$$$$$       /$$$$$$$   /$$$$$$  /$$   /$$ /$$$$$$$$ /$$      
     /$$__  $$ /$$__  $$| $$$    /$$$| $$_____/      | $$__  $$ /$$__  $$| $$$ | $$| $$_____/| $$      
    | $$  \__/| $$  \ $$| $$$$  /$$$$| $$            | $$  \ $$| $$  \ $$| $$$$| $$| $$      | $$      
    | $$ /$$$$| $$$$$$$$| $$ $$/$$ $$| $$$$$         | $$$$$$$/| $$$$$$$$| $$ $$ $$| $$$$$   | $$      
    | $$|_  $$| $$__  $$| $$  $$$| $$| $$__/         | $$____/ | $$__  $$| $$  $$$$| $$__/   | $$      
    | $$  \ $$| $$  | $$| $$\  $ | $$| $$            | $$      | $$  | $$| $$\  $$$| $$      | $$      
    |  $$$$$$/| $$  | $$| $$ \/  | $$| $$$$$$$$      | $$      | $$  | $$| $$ \  $$| $$$$$$$$| $$$$$$$$
     \______/ |__/  |__/|__/     |__/|________/      |__/      |__/  |__/|__/  \__/|________/|________/
""")
    print("Ver 0.1")

def logoo():
    print(""" 


            """,start.login,"""
            """,start.reg,"""


    """)

class start(object):
    login = """
    ╦  ╔═╗╔═╗ ╦ ╔╗╔
    ║  ║ ║║ ╦ ║ ║║║
    ╩═╝╚═╝╚═╝ ╩ ╝╚╝
    """
    reg = """
    ╦═╗╔═╗╔═╗╦╔═╗╔╦╗╔═╗╦═╗
    ╠╦╝║╣ ║ ╦║╚═╗ ║ ║╣ ╠╦╝
    ╩╚═╚═╝╚═╝╩╚═╝ ╩ ╚═╝╩╚═
    """
    s_login = """
          __   __             
    |    /  \ / _` | |\ |    .
    |___ \__/ \__> | | \|    .
"""
    s_password = """


     __        __   __        __   __   __         
    |__)  /\  /__` /__` |  | /  \ |__) |  \    .   
    |    /~~\ .__/ .__/ |/\| \__/ |  \ |__/    .   
                                                


    """


def login_page():
    print(""" 


            """,start.login,"""
            """,start.reg,"""


    """)
    choice = input(">")
    while True:
        if choice in ("help","help()"):
            clean()
            login_page(choice)
            print("> Enter 'login' to login to your account or 'register' to register a new")
            time.sleep(3)
            clean()
            login_page()
        if choice in ("log", "login", "log()", "login()"):
            clean()
            login_page()
            print(">login")
            time.sleep(3)
            clean()
            login_page()

class curr_user(object):
    def __init__(self,ids,login,password,name,message,time,access,level,cash,debt,task,status):
        self.ids = ids
        self.login = login 
        self.password = password
        self.name = name
        self.message = message
        self.time = time
        self.access = access
        self.level = level
        self.cash = cash
        self.debt = debt
        self.task = task
        self.status = status

def db_encoded_get(query):
    predata = db_get(query)
    soft_data = predata.replace("'","", 2)
    lst = ()
    soft_data = soft_data.replace("(","").replace(")","")
    if len(soft_data) > 3:
        soft_data = soft_data.split(",")
    else:
        return (int(soft_data[0:2]),)
    for x in soft_data:
        lst = lst + (int(x), )
    return lst 

def db_get_user(user_login):
    try:
        value_ids = db_get("SELECT id FROM users WHERE login = '{}'".format(user_login))
        value_login = db_get("SELECT login FROM users WHERE login = '{}'".format(user_login))
        value_password = decode(db_encoded_get("SELECT pass FROM users WHERE login = '{}'".format(user_login)))
        value_name = db_get("SELECT name FROM users WHERE login = '{}'".format(user_login))
        value_message = decode(db_encoded_get("SELECT message FROM users WHERE login = '{}'".format(user_login)))
        value_time = decode(db_encoded_get("SELECT time FROM users WHERE login = '{}'".format(user_login)))
        value_access = decode(db_encoded_get("SELECT access FROM users WHERE login = '{}'".format(user_login)))
        value_level = decode(db_encoded_get("SELECT level FROM users WHERE login = '{}'".format(user_login)))
        value_cash = decode(db_encoded_get("SELECT cash FROM users WHERE login = '{}'".format(user_login)))
        value_debt = decode(db_encoded_get("SELECT debt FROM users WHERE login = '{}'".format(user_login)))
        value_task = decode(db_encoded_get("SELECT task FROM users WHERE login = '{}'".format(user_login)))
        value_status = db_get("SELECT status FROM users WHERE login = '{}'".format(user_login))
        user = curr_user(value_ids , value_login , value_password , value_name , value_message , value_time , value_access , value_level , value_cash , value_debt, value_task, value_status)
        return user
    except:
        return None

def if_user_exist():
    inp_login = allowed(enter_login)
    user_data = db_get("SELECT login FROM users WHERE login = '{}'".format(inp_login))
    if len(user_data) > 0:
        return inp_login
    else:
        return None
    return inp_login
    
def password_check(login,password=None):
    if password == None:
        inp_pass = allowed(enter_password)
    else:
        inp_pass = password
    val_db = db_encoded_get("SELECT pass FROM users WHERE login = '{}'".format(login))
    if encode(inp_pass) == val_db:
        return True
    else:
        return False

def allowed(text):
    try:
        if text != "":
            print(text)
        value = input("> ")
        if value != "" and len(value) > 4:
            return value
        else:
            clean()
            value = allowed(text)
    except:
        value = allowed(text)
    return value

def db_get_task(ids):
    task_id = ids
    task_name =  db_get("SELECT name FROM tasks WHERE id = '{}'".format(ids))
    task_description = db_get("SELECT description FROM tasks WHERE id = '{}'".format(ids))
    task_reward = decode(db_encoded_get("SELECT reward FROM tasks WHERE id = '{}'".format(ids)))
    task_pending = db_get("SELECT pending FROM tasks WHERE id = '{}'".format(ids))
    task_status = db_get("SELECT status FROM tasks WHERE id = '{}'".format(ids))
    return task_id, task_name, task_description, task_reward, task_pending, task_status

def db_get_debt(ids):
    

def db_get_game(game_id=None, access=None):
    if game_id == None:
        game = db_get("SELECT name FROM games WHERE access = {};".format(access))
        return game
    if access == None:
        game = db_get("SELECT name FROM games WHERE id = {};".format(game_id))
        return game

def clean():
    os.system("cls")

def encode(some):
    return tuple([ord(x) for x in some])

def decode(some):
    pre = [chr(x) for x in some]
    s = ""
    for x in pre:
        s = s + x
    return s

def seconds():
        str_sec = int(strftime("%S", gmtime()))
        str_min = int(strftime("%M", gmtime()))
        str_hr = int(strftime("%H", gmtime()))
        str_total_sec = str_sec + (str_min * 60) + (str_hr * 3600)
        return str_total_sec

def db_get(query):
    try:
        connection = sqlite3.connect(db_name)
        db = connection.cursor()
        data = ()
        for values in db.execute(query):
            data += (values, )
        db.close()
        if len(data) == 1:
            return data[0][0]
        else:
            return data
    except Exception as error:
        return error

def db_execute(query):
    try:
        connection = sqlite3.connect(db_name)
        db = connection.cursor()
        db.execute(query)
        connection.commit()
        connection.close()
    except Exception as error:
        return error

def login_fun(login=None, password=None):
    if login == None or password == None:
        user_login = if_user_exist()
        if user_login != None:
            inp_password  = password_check(user_login)
            if inp_password == True:
                user = db_get_user(user_login)
                if user == "error":
                    return "Error!"
                if user == "user don't exist":
                    return "not found"
                else:
                    return user
            if inp_password == False:
                return "Incorrect password"
        else:
            return "No such user"
    elif login != None and password != None:
        inp_password  = password_check(login,password)
        if inp_password == True:
            user = db_get_user(login)
            if user == "error":
                return "Error!"
            if user == "user don't exist":
                return "not found"
            else:
                return user
        if inp_password == False:
            return "Incorrect password"
    else:
        return "No such user" 

def interface(u,num):
    if num == 0:
        print("""
            Пользователь : """,u.name,"""

        1) Информация

        2) Задания

        3) Список игр

        4) Долг

        5) Сообщение

        6) Сменить пароль
            """)
    
    if num == 1:
        print("""
        Пользователь : """,u.name,"""

        Имя : """,u.name,"""
        Логин : """,u.login,"""
        """,u.time,""" секунд времени осталось
        """,u.access,""" уровень доступа
        """,u.level,""" уровень
        Баланс : """,u.cash,"""
        Долг : """,u.debt,"""
        Задание : """,u.task,
        )
        
    if num == 2:
        task_id, task_name, task_description, task_reward, task_pending, task_status = db_get_task(u.task)
        if task_pending == 1:
            print("""
            Пользователь : """,u.name,"""

            Текущее задание : """,task_name,"""

            Описание : """,task_description,"""
            
            Награда : """,task_reward,"""
            """
            )
            print("""
            Готово?(да/нет)
            """)
            choice = input("> ")
            if choice == "да":
                db_execute("UPDATE tasks SET status = 3, pending = 0 WHERE id = {};".format(task_id))
                сlear()
            else:
                pass

        if task_status == 1:
            print("""
            Пользователь : """,u.name,"""

            Текущее задание : """,task_name,"""

            Описание : """,task_description,"""
            
            Награда : """,task_reward,"""
            """
            )
            print("""
            Взять задание?
            Вы получите """,task_reward,"""
            """)
            db_execute("UPDATE tasks SET status = 2, pending = 1 WHERE id = {};".format(task_id))
            

        if task_status == 3:
            print("""
            Пользователь : """,u.name,"""

            Текущее задание : """,task_name,"""

            Описание : """,task_description,"""
            
            Награда : """,task_reward,"""
            """
            )
            print("""
            Задание выполнено, ожидайте подтверждения!
            """)
            
            
        if task_status == 4:
            print("""
            Пользователь : """,u.name,"""           

            Текущее задание : """,task_name,"""

            Описание : """,task_description,"""
            
            Награда : """,task_reward,"""
            """
            )
            print("""
            Задание выполнено и подтвержено!
            Вы получили """,task_reward,"""
            """)
            db_execute("UPDATE tasks SET status = 5, pending = 0 WHERE id = {};".format(task_id))
            
        
        if task_status == 5:
            print("""
            Пользователь : """,u.name,"""

            Текущее задание : """,task_name,"""

            Описание : """,task_description,"""
            
            Награда : """,task_reward,"""
            """
            )
            print("""
            Вы уже получили награду!
            """)

    if num == 3:
        game = db_get_game(None,u.access)
        count = 0
        print("""
            Пользователь : """,u.name,"""
            Доступные игры :"""
            )
        if isinstance(game, str) == True:
            print("""
            """,game
                )
        else:
            while count < len(game):
                print("""
            ""","{})".format(count),game[count][0])
                count += 1

    if num == 4:
        print("""
            Пользователь : """,u.name
            )
        if int(u.debt) == 0:
             print("""
             Долга нет! Ура!
             """)
        else:
            print("""
            Долг : """,u.debt,"""
            
            """) 


def user_menu(u):
    if u.status == 1:
        while True:
            interface(u,0)
            try:
                choice = int(input("> "))
            except:
                clean()
                user_menu(u)
            if choice in range(0,8):
                clean()
                interface(u,choice)
                input("> ")
                clean()
            else:
                pass

    elif u.status == 2:
        print("Заблокировано! ")
    elif u.status == 3:
        print("")
    else:
        print("Ошибка!")


def main(): 
    clean()
    logo()
    time.sleep(1)
    clean()
    user_login, user_password  = main_page()
    
    user = login_fun(user_login, user_password)
    if isinstance(user, curr_user) == True:
        user_menu(user)
    else:
        print("User not found or password is incorrect")
    time.sleep(20)

def main_page():
    logoo()
    choice = input("> ")
    if choice in ("help","help()"):
        clean()
        logoo()
        print("> Enter 'login' to login to your account or 'register' to register a new")
        time.sleep(3)
        clean()
        user_login, user_password = main_page()

    if choice in ("log", "login", "log()", "login()"):
        clean()
        logoo()
        print("> logining()")
        time.sleep(1)
        clean()
        user_login = allowed(start.s_login)
        clean()
        print(start.s_login)
        print("\n>", user_login)
        user_password = allowed(start.s_password)
        clean()
        return user_login, user_password

    else:
        clean()
        logoo()
        print("> Type 'help' to view commands ")
        time.sleep(3)
        clean()
        user_login, user_password = main_page()
    return user_login, user_password

main()
